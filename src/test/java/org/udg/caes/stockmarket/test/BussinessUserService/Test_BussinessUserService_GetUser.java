package org.udg.caes.stockmarket.test.BussinessUserService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_BussinessUserService_GetUser {


    @Mocked
    User u;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessUserService bUS;

    @Test
    public void test_complete() throws EntityNotFound {
        new NonStrictExpectations(){{
            mPS.getUser(anyString);times=1;result=u;
        }};
        bUS.getUser("user");
    }

    @Test(expected = EntityNotFound.class)
    public void test_wrong_EntityNotFound() throws EntityNotFound {
        new NonStrictExpectations(){{
            mPS.getUser(anyString);times=1;result=new EntityNotFound();
        }};
        bUS.getUser("user");
    }

}
