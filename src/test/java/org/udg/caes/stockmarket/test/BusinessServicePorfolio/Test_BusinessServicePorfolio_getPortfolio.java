package org.udg.caes.stockmarket.test.BusinessServicePorfolio;

import mockit.*;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import static org.junit.Assert.assertEquals;

public class Test_BusinessServicePorfolio_getPortfolio {

    @Mocked
    User u;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessService bs;

    @Test
    public void getPortfolio_correct() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = new Portfolio(u,"id");
        }};

        Portfolio portfolio = bs.getPortfolio("","id");
        assertEquals(portfolio.getId(), "id");
  }

    @Test(expected = ElementNotExists.class)
    public void getPortfolio_Exception_ElementNotExist() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = new ElementNotExists();
        }};

        bs.getPortfolio("","id");
    }

    @Test(expected = EntityNotFound.class)
    public void getPortfolio_Exception_EntityNotFound() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = new EntityNotFound();

        }};

        bs.getPortfolio("","id");
    }
}
