package org.udg.caes.stockmarket.test.User;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Created by jasab_000 on 27/01/2015.
 */
public class Test_User_HasPortfolio {
    User u;

    @Before
    public void setUp(){
        u = new User ("u1");
    }

    @Test
    public void test_completed_true(
            @Mocked final Portfolio p) throws ElementNotExists {

        new NonStrictExpectations(u){{
            p.getId(); times=2;
            returns ("wrong" ,"found");
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p, p));
        }};

        assertTrue(u.hasPortfolio("found"));

    }

    @Test
    public void test_completed_false(
            @Mocked final Portfolio p) throws ElementNotExists{

        new NonStrictExpectations (u){{
            p.getId(); times=2;
            returns ("wrong" ,"notOk");
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p,p));
        }};

        assertTrue(!u.hasPortfolio("notFound"));

    }

}