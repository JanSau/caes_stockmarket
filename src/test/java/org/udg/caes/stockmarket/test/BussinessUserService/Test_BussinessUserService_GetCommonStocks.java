package org.udg.caes.stockmarket.test.BussinessUserService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Assert;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_BussinessUserService_GetCommonStocks {

    @Mocked
    User u;
    @Mocked
    Portfolio p;
    @Mocked
    Stock s;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessUserService bUS;

    @Test
    public void test_complete() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser(anyString);times=2;result=u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p, p);
            p.getAllStocks();times=2;result=Arrays.asList(s,s);

            s.getName();times=11; returns("s1","s1","s1","s2","s2","s2","s3","s3","s3","s2","s2","s2");

            u.hasStock("s1");times=1;result=true;
            u.hasStock("s2");times=2;result=true;
            u.hasStock("s3");times=1;result=false;

        }};
        assertEquals("Llista esperada de mida: ",3,bUS.getCommonStocks("id1", "id2").size(),0);

    }

    @Test
    public void test_complete_emptyList() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser(anyString);times=2;result=u;
            u.getAllPortfolios();times=1;result= new ArrayList<Portfolio>();
        }};
        assertEquals("Llista esperada de mida: ",0,bUS.getCommonStocks("id1", "id2").size(),0);

    }

    @Test(expected = EntityNotFound.class)
    public void test_wrong_EntityNotFound() throws Exception {

        new NonStrictExpectations(){{
            mPS.getUser("id");result= new EntityNotFound();
        }};
        bUS.getCommonStocks("id", "id2");

    }

}
