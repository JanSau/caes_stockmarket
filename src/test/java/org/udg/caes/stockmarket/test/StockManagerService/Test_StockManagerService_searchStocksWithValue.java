package org.udg.caes.stockmarket.test.StockManagerService;

import mockit.*;
import org.junit.Test;
import org.udg.caes.stockmarket.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by imartin on 20/12/13.
 */
public class Test_StockManagerService_searchStocksWithValue {

  @Injectable
  PersistenceService ps;
  @Injectable
  StockService ss;

  @Tested
  StockManagerService sMS;



  @Test
  public void test_no_markets() {

    new NonStrictExpectations() {{
      ss.getMarkets();
      result = new ArrayList<StockMarket>();
    }};

    List<String> stocks = sMS.searchStocksWithValue(100.0);

    assertEquals("List should be empty", 0, stocks.size());
  }

  @Test
  public void test_all_lower(@Mocked final StockMarket sm) throws Exception {

    final StockMarket sm1 = new MockUp<StockMarket>() {
      @Mock
      ArrayList<String> getAllStocks() {
        return new ArrayList<String>() {{ add("NVDA"); }};
      }
    }.getMockInstance();

    new NonStrictExpectations() {{
      ss.getMarkets();
      result = new ArrayList<StockMarket>() {{ add(sm1); }};
      ss.getPrice("NVDA");
      result = 90.0;
    }};

    List<String> stocks = sMS.searchStocksWithValue(100.0);

    assertEquals("List should be empty", 0, stocks.size());
  }

  @Test
  public void test_one_lower_one_higher(@Mocked final StockMarket sm) throws Exception {

    final StockMarket sm1 = new MockUp<StockMarket>() {
      @Mock
      ArrayList<String> getAllStocks() {
        return new ArrayList<String>() {{ add("NVDA"); add("FB"); }};
      }
    }.getMockInstance();

    new NonStrictExpectations() {{
      ss.getMarkets();
      result = new ArrayList<StockMarket>() {{ add(sm1); }};
      ss.getPrice("NVDA");
      result = 90.0;
      ss.getPrice("FB");
      result = 110.0;

    }};

    List<String> stocks = sMS.searchStocksWithValue(100.0);

    assertEquals("List should contain 1 stock", 1, stocks.size());
    assertEquals("Stock found should be FB", "FB", stocks.get(0));
  }



}
