package org.udg.caes.stockmarket.test.BussinessUserService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Test;
import org.udg.caes.stockmarket.BusinessUserService;
import org.udg.caes.stockmarket.PersistenceService;
import org.udg.caes.stockmarket.StockService;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_BussinessUserService_SaveUser {


    @Mocked
    User u;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessUserService bUS;

    @Test
    public void test_complete() throws EntityNotFound {
        new NonStrictExpectations(){{
            mPS.saveUser(u);times=1;
        }};
        bUS.saveUser(u);
    }

}
