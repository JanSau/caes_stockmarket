package org.udg.caes.stockmarket.test.User;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.Stock;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

/**
 * Test Class
 * Created by Mar on 23/01/2015.
 */

public class Test_User_getPortfolio {

    User u;

    @Before
    public void setUp(){
        u = new User ("u1");
    }

    @Test
    public void test_completed(
            @Mocked final Portfolio p) throws ElementNotExists{

        new NonStrictExpectations (u){{
            p.getId(); times=2;
            returns ("wrong" ,"found");
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p,p));
        }};

        u.getPortfolio("found");

    }

    @Test (expected= ElementNotExists.class)
    public void test_userGetPortfolio_ElementNotExists(
            @Mocked final Portfolio p) throws ElementNotExists{

        new NonStrictExpectations (u){{
            p.getId(); times=2;
            returns ("wrong" ,"notOk");
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p,p));
        }};

        u.getPortfolio("notFound");

    }

}
