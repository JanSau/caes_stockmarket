package org.udg.caes.stockmarket.test.BussinessUserService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_BussinessUserService_HasStock {

    @Mocked
    User u;
    @Mocked
    Portfolio p;
    @Mocked
    Stock s;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessUserService bUS;

    @Test
    public void test_complete_true() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser("id");times=1;result=u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p, p);
            p.hasStock("ticker");times=1;result=true;
        }};
        assertTrue(bUS.hasStock("id", "ticker"));
    }

    @Test
    public void test_complete_false() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser("id");times=1;result=u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p, p);
            p.hasStock("ticker");times=2;result=false;
        }};
        assertTrue(!bUS.hasStock("id", "ticker"));
    }

    @Test(expected = EntityNotFound.class)
    public void test_wrong_EntityNotFound() throws Exception {

        new NonStrictExpectations(){{
            mPS.getUser("id");result= new EntityNotFound();
        }};
        bUS.hasStock("id", "ticker");

    }
}
