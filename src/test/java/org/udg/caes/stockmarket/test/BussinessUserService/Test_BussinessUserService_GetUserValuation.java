package org.udg.caes.stockmarket.test.BussinessUserService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Assert;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.Arrays;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_BussinessUserService_GetUserValuation {

    @Mocked
    User u;
    @Mocked
    Portfolio p;
    @Mocked
    Stock s;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessUserService bUS;

    @Test
    public void test_complete() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser("id");times=1;result=u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p,p);
            p.getAllStocks();times=2;result=Arrays.asList(s,s);
            s.getQuantity();times=4;result=1;
            mSS.getPrice(anyString);times=4;result=1;
        }};
        double d=bUS.getUserValuation("id");
        Assert.assertEquals("User valuation: ",d,4,0);

    }

    @Test(expected = EntityNotFound.class)
    public void test_wrong_EntityNotFound() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser("id");times=1;result=new EntityNotFound();
        }};
        double d=bUS.getUserValuation("id");

    }

    @Test(expected = StockNotFound.class)
    public void test_wrong_StockNotFound() throws Exception {
        new NonStrictExpectations(){{
            mPS.getUser("id");times=1;result=u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p,p);
            p.getAllStocks();times=1;result=Arrays.asList(s,s);
            s.getQuantity();times=1;result=1;
            mSS.getPrice(anyString);times=1;result=new StockNotFound();
        }};
        double d=bUS.getUserValuation("id");

    }

}
