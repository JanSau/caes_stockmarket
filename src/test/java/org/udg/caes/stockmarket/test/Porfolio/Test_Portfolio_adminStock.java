package org.udg.caes.stockmarket.test.Porfolio;

import mockit.Mocked;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.Stock;
import org.udg.caes.stockmarket.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class Test_Portfolio_adminStock {

    @Mocked
    User u;

    @Test
    public void testSimple_addStockCorrect(@Mocked final Stock s) throws Exception {
        Portfolio port = new Portfolio(u,"");
        assertEquals(port.getAllStocks().size(),0);
        port.addStock(s);
        assertEquals(port.getAllStocks().size(), 1);
        port.addStock(s);
        assertEquals(port.getAllStocks().size(),2);
    }

    @Test
    public void testSimple_hasStockCorrect() throws Exception {
        Portfolio port = new Portfolio(u,"");
        Stock s = new Stock("NVDA",200,4000);
        port.addStock(s);
        assertTrue(port.hasStock(s.getName()));
    }

    @Test
    public void testSimple_hasStockInCorrect() throws Exception  {
        Portfolio port = new Portfolio(u,"");
        assertTrue(!port.hasStock("N"));
    }
}
