package org.udg.caes.stockmarket.test.OrderManagerService;

import mockit.*;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.persistence.Fake_PS_MySQL;

/**
 * Created by jasab_000 on 27/01/2015.
 */
public class Test_OrderManagerService_placeOrder {



    @Mocked
    Order o;
    @Mocked
    Fake_PS_MySQL ps;
    @Mocked
    Portfolio p;
    @Mocked
    User u;
    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    OrderManagerService oMS;

    @Test
    public void test_completed() throws Exception {

        new NonStrictExpectations() {{
            o.setId(anyString); times=1;
            o.setStatus(Order.PROCESSING); times=1;
            mPS.saveOrder(o);times=1;
            o.send(mSS); times=1;
        }};

        oMS.placeOrder(o);

    }


    @Test(expected = NoSuchMethodException.class)
    public void test_NoSuchMethodException() throws Exception {

        new NonStrictExpectations() {{
            o.setId(anyString); times=1;
            o.setStatus(Order.PROCESSING); times=1;
            mPS.saveOrder(o);times=1;
            o.send(mSS); times=1; result= new NoSuchMethodException();
        }};

        oMS.placeOrder(o);

    }

}
