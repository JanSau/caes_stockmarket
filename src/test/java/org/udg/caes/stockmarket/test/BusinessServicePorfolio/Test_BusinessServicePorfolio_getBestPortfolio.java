package org.udg.caes.stockmarket.test.BusinessServicePorfolio;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class Test_BusinessServicePorfolio_getBestPortfolio {

    @Mocked
    User u;

    @Mocked
    Stock s;

    @Mocked
    Portfolio p;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessService bs;

    @Test
    public void test_getBestPortfolio() throws Exception {

        //TO DO
        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getAllPortfolios();times=1;result= Arrays.asList(p,p);
            p.getAllStocks();times=2;result=Arrays.asList(s,s);
            s.getQuantity();result=1;
            s.getName();times=4;returns("s","s","ss","s");
            mSS.getPrice("s");times=3;result=1;
            mSS.getPrice("ss");times=1;result=6;
            s.getBuyPrice();times=4;result=1;
        }};

        Portfolio portfolio = bs.getBestPortfolio("idUser");

    }

    @Test(expected = StockNotFound.class)
    public void getBestPortfolio_Exception_StockNotFound() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>() {{ add(p);}};
            p.getAllStocks();
            result = new ArrayList<Stock>() {{ add(s);}};
            s.getQuantity();
            result = anyInt;
            mSS.getPrice(anyString);
            result = new StockNotFound();

        }};
        bs.getBestPortfolio("idUser");
    }

    @Test
    public void getBestPortfolio_EmptyList() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>() {{ }};
        }};

        Portfolio p = bs.getBestPortfolio("idUser");
        assertTrue(p==null);
    }

    @Test(expected = EntityNotFound.class)
    public void getBestPortfolio_Exception_EntityNotFound() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = new EntityNotFound();
        }};

        bs.getBestPortfolio("idUser");
    }


}
