package org.udg.caes.stockmarket.test.User;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.OrderManagerService;
import org.udg.caes.stockmarket.PersistenceService;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jasab_000 on 28/01/2015.
 */
public class Test_User_AddPortfolio {

    User u;

    @Before
    public void setUp(){
        u = new User ("u1");
    }

    @Test
    public void test_completed(
            @Mocked final Portfolio p) throws ElementAlreadyExists {

        new NonStrictExpectations(u) {{
            p.getId();
            times = 1;
            returns("idPortfolio");
            u.hasPortfolio(anyString);times=1;result=false;
        }};

        u.addPortfolio(p);

    }


    @Test(expected = ElementAlreadyExists.class)
    public void test_wrong_ElementAlreadyExists(
            @Mocked final Portfolio p) throws ElementAlreadyExists {

        new NonStrictExpectations(u) {{
            p.getId();
            times = 1;
            returns("idPortfolio");
            u.hasPortfolio(anyString);result=true;
        }};

        u.addPortfolio(p);

    }

}
