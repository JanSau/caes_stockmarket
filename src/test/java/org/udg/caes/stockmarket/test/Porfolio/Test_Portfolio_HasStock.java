package org.udg.caes.stockmarket.test.Porfolio;

import mockit.*;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by jasab_000 on 27/01/2015.
 */
public class Test_Portfolio_HasStock {



    @Mocked User u;

    Portfolio p;
    @Before
    public void init(){
        p=new Portfolio(u,"portfolio");
    }


    @Test
    public void test_compelteTrue(@Mocked final Stock s, @Mocked User u){

        new NonStrictExpectations(p){{
            p.getAllStocks(); result= Arrays.asList(s,s);
            s.getName();times = 2; returns ("wrong","stock");
        }};
        assertTrue(p.hasStock("stock"));
    }

    @Test
    public void test_compelteFalse(@Mocked final Stock s, @Mocked User u){

        new NonStrictExpectations(p){{
            p.getAllStocks(); result= Arrays.asList(s,s);
            s.getName();times = 2; returns ("wrong","notOk");
        }};
        assertTrue(!p.hasStock("stock"));
    }

}