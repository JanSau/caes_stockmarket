package org.udg.caes.stockmarket.test.StockManagerService;

import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Tested;
import org.junit.Test;
import org.udg.caes.stockmarket.*;

import java.lang.reflect.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by imartin on 19/12/13.
 */
public class Test_StockManagerService_searchStocks {

  @Injectable
  PersistenceService ps;
  @Injectable
  StockService ss;

  @Tested
  StockManagerService us;

  @Test
  public void test_no_markets() {

    new NonStrictExpectations() {{
      ss.getMarkets(); result = new ArrayList<StockMarket>();
    }};

    List<String> stocks = us.searchStocks("NVDA");

    assertEquals("List should be empty", 0, stocks.size());
  }


}
