package org.udg.caes.stockmarket.test.BusinessServicePorfolio;

import mockit.*;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

public class Test_BusinessServicePorfolio_getPortfolioValuation {

    @Mocked
    User u;

    @Mocked
    Stock s;

    @Mocked
    Portfolio p;

    @Injectable
    PersistenceService mPS;
    @Injectable
    StockService mSS;

    @Tested
    BusinessService bs;

    @Test
    public void getPortfolioValuation_0_stocks() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = p;
            p.getAllStocks();
            result = new ArrayList<Stock>() {{ }};
        }};
        Double v = bs.getPortfolioValuation("idUser","id");
        assertTrue(v==0.0);
    }

    @Test
    public void getPortfolioValuation_correct_1_stock() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = p;
            p.getAllStocks();
            result = new ArrayList<Stock>() {{ add(s);}};
            s.getQuantity();
            result = 100;
            mSS.getPrice(anyString);
            result = 6.0;
        }};

        Double v = bs.getPortfolioValuation("idUser","id");
        assertTrue(v==600.0);
    }

    @Test
    public void getPortfolioValuation_correct_3_stocks() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = p;
            p.getAllStocks();
            result = new ArrayList<Stock>() {{ add(s); add(s); add(s);}};
            s.getQuantity();
            result = 100;
            mSS.getPrice(anyString);
            result = 6.0;
        }};

        Double v = bs.getPortfolioValuation("idUser","id");
        assertTrue(v==1800.0);
    }

    @Test(expected = EntityNotFound.class)
    public void getPortfolioValuation_Exception_EntityNotFound() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = new EntityNotFound();
        }};

        Double v = bs.getPortfolioValuation("idUser","id");
    }

    @Test(expected = ElementNotExists.class)
    public void getPortfolioValuation_Exception_ElementNotExists() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = new ElementNotExists();
        }};

        Double v = bs.getPortfolioValuation("idUser","id");
    }

    @Test(expected = StockNotFound.class)
    public void getPortfolioValuation_Exception_StockNotFound() throws Exception {

        new NonStrictExpectations() {{
            mPS.getUser(anyString); times =1;
            result = u;
            u.getPortfolio(anyString); times =1;
            result = p;
            p.getAllStocks();
            result = new ArrayList<Stock>() {{ add(s);}};
            s.getQuantity();
            result = 100;
            mSS.getPrice(anyString);
            result = new StockNotFound();
        }};

        Double v = bs.getPortfolioValuation("idUser","id");
    }

}
