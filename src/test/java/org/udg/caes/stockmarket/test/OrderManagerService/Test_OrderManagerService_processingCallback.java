package org.udg.caes.stockmarket.test.OrderManagerService;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.persistence.Fake_PS_MySQL;

/**
 * Created by jasab_000 on 27/01/2015.
 */
public class Test_OrderManagerService_processingCallback {
    @Mocked
    Order o;
    @Mocked
    PersistenceService ps;

    @Test
    public void test_completed() throws Exception {

        new MockUp<OrderManagerService>(){
            @Mock
            PersistenceService getPersistenceService() { return ps; }
        };

        new NonStrictExpectations() {{
            ps.saveOrder(o); times=1;
        }};

        OrderManagerService.processingCallback(o);

    }
}
