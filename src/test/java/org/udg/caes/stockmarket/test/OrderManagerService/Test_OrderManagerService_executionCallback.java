package org.udg.caes.stockmarket.test.OrderManagerService;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.persistence.Fake_PS_MySQL;

/**
 * Created by imartin on 20/12/13.
 */
public class Test_OrderManagerService_executionCallback {

  @Mocked
  Order o;
  @Mocked
  PersistenceService ps;
  @Mocked
  Portfolio p;
  @Mocked
  User u;

  @Test
  public void test_completed() throws Exception {

      final Stock s = new MockUp<Stock>() {
          @Mock
          String getName() { return "NVDA"; }
          @Mock
          int getQuantity() { return 100; }
          @Mock
          double getPrice() { return 14.7; }
      }.getMockInstance();

      new MockUp<OrderManagerService>(){
          @Mock
          PersistenceService getPersistenceService() { return ps; }
      };

    new NonStrictExpectations() {{
      o.getStatus();
      result = Order.COMPLETED;
      o.getEfPrice();
      o.getEfQuant();
      o.getTicker();
      new Stock(anyString, anyInt, anyDouble);
      result = s;
      o.getPortfolio();
      result = p;
      p.addStock(s);
      ps.saveOrder(o);
    }};

    OrderManagerService.executionCallback(o);

  }
}
