package org.udg.caes.stockmarket.test.User;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.OrderManagerService;
import org.udg.caes.stockmarket.PersistenceService;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jasab_000 on 27/01/2015.
 */
public class Test_User_ReplacePortfolio {

    User u;

    @Before
    public void setUp(){
        u = new User ("u1");
    }

    @Test
    public void test_completed(
            @Mocked final Portfolio p) throws ElementNotExists {

        new NonStrictExpectations(u) {{
            p.getId();
            times = 4;
            returns("wrong", "found","found", "found");
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p, p));
        }};

        u.replacePortfolio(p);

    }

    @Test(expected=ElementNotExists.class)
    public void test_wrong_ElementNotExists(
            @Mocked final Portfolio p) throws ElementNotExists {

        new NonStrictExpectations(u) {{
            u.getAllPortfolios();
            result = new ArrayList<Portfolio>(Arrays.asList(p, p));
            p.getId();
            times = 4;
            returns("wrong", "found","wrong", "found");
        }};

        u.replacePortfolio(p);

    }

}
