package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.exceptions.InvalidOperation;
import org.udg.caes.stockmarket.exceptions.StockNotFound;
import org.udg.caes.stockmarket.exceptions.InvalidOperation;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 22/10/13
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */
public interface StockMarket {
  public String getName();
  public Double getPrice(String stockName) throws StockNotFound;
  public Operation buy(String name, int quantity) throws StockNotFound, InvalidOperation;
  public Operation sell(String name, int quantity) throws Exception;

  boolean hasStock(String name);

  ArrayList<String> getAllStocks();
}
