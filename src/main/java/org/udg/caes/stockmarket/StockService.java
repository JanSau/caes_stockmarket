package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.exceptions.InvalidOperation;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 */
public interface StockService {
  public String getName();
  public Operation buy(String name, int quantity) throws StockNotFound, InvalidOperation;
  public Double getPrice(String name) throws StockNotFound;

  public Collection<StockMarket> getMarkets();

  public boolean send(Order o, Method m, Method processingCallback);
}
