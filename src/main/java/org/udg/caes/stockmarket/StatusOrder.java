package org.udg.caes.stockmarket;

public class StatusOrder {

    public static StatusOrder ACCEPTED = new StatusOrder(1);
    public static StatusOrder PROCESSING = new StatusOrder(2);
    public static StatusOrder COMPLETED = new StatusOrder(3);
    public static StatusOrder PARTIALLY_COMPLETED = new StatusOrder(4);
    public static StatusOrder CANCELLED = new StatusOrder(5);
    public static StatusOrder TIMEOUT = new StatusOrder(6);
    public static StatusOrder REJECTED = new StatusOrder(7);
    private static final StatusOrder[] values = {ACCEPTED, PROCESSING, COMPLETED, PARTIALLY_COMPLETED,CANCELLED,TIMEOUT, REJECTED};

    private final int code;

    private StatusOrder (int code_status) {
        code = code_status;
    }

    public int getCode() {
        return code;
    }

    public static StatusOrder code(int arg) {
        return values[arg];
    }
}
