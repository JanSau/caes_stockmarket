package org.udg.caes.stockmarket;

import java.util.Date;

/**
 * Created by imartin on 12/12/13.
 */
public class Order {
  private String id;

  Portfolio portfolio;

  int quantity;
  double targetPrice;
  Date limit;
  String ticker;

  StatusOrder status;
  public static int ACCEPTED = StatusOrder.ACCEPTED.getCode();
  public static int PROCESSING = StatusOrder.PROCESSING.getCode();
  public static int COMPLETED = StatusOrder.COMPLETED.getCode();
  public static int PARTIALLY_COMPLETED = StatusOrder.PARTIALLY_COMPLETED.getCode();
  public static int CANCELLED = StatusOrder.CANCELLED.getCode();
  public static int TIMEOUT = StatusOrder.TIMEOUT.getCode();
  public static int REJECTED = StatusOrder.REJECTED.getCode();

  private int efQuant;
  private double efPrice;

  Order(Portfolio pf, int q, Date l, String t) {
    status = StatusOrder.code(2);
    portfolio = pf;
    quantity = q;
    ticker = t;
    limit = l;
  }


  /**
   * Send the order and a callback to be used once the stock service receives the response from the broker
   * @param ss
   * @return true if the order has been accepted
   * @throws NoSuchMethodException
   */
  public boolean send(StockService ss) throws NoSuchMethodException {
    return ss.send(this,
        OrderManagerService.class.getDeclaredMethod("processingCallback", Order.class),
        OrderManagerService.class.getDeclaredMethod("executionCallback", Order.class));
  }

  public String getId() {
    return id;
  }

  public void setId(String i) {
    this.id = i;
  }

  public String getTicker() {
    return ticker;
  }

  public int getEfQuant() {
    return efQuant;
  }

  public double getEfPrice() {
    return efPrice;
  }

  public Portfolio getPortfolio() {
    return portfolio;
  }

  public int getStatus() {
    return status.getCode();
  }

  public void setEfQuant(int eq) { this.efQuant = eq; }

  public void setEfPrice(double ep) {
    this.efPrice = ep;
  }

  public void setStatus(int s) {
    this.status = StatusOrder.code(s);
  }

  public int getQuantity() {
    return quantity;
  }


}
