package org.udg.caes.stockmarket;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: 
 * Date: 22/10/13
 * Time: 13:33
 * To change this template use File | Settings | File Templates.
 */
public class Portfolio {
  private String portfolioID;
  private User mUser;
  private List<Stock> mStocks = new ArrayList<Stock>();

  public Portfolio(User user, String id_porfolio) {
    mUser = user;
    portfolioID = id_porfolio;
  }

  public User getUser() {
    return mUser;
  }

  public void setUser(User mUser) {
    this.mUser = mUser;
  }

  public String getId() {
    return portfolioID;
  }

  public void setId(String id_portfolio) {
    this.portfolioID = id_portfolio;
  }

  public List<Stock> getAllStocks() {
    return mStocks;
  }

  public void setStocks(List<Stock> stocks) {
    this.mStocks = stocks;
  }

  public void addStock(Stock stock) {
    mStocks.add(stock);
  }

  public Boolean hasStock(String ticker) {
    for (Stock s: getAllStocks())
      if (s.getName().equals(ticker))
        return true;
    return false;
  }
}
