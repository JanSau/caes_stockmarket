package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 12/11/13
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */
public class User {
    String userID;
    List<Portfolio> portfolios = new ArrayList<Portfolio>();

    public User(String id) {
        userID = id;
    }

    public boolean hasPortfolio(String id_portfolio) {
        for (Portfolio p: getAllPortfolios())
            if (p.getId().equals(id_portfolio))
                return true;
        return false;
    }

    public Portfolio getPortfolio(String id_portfolio) throws ElementNotExists {
        for (Portfolio p: getAllPortfolios())
            if (p.getId().equals(id_portfolio))
                return p;
        throw new ElementNotExists();
    }

    public List<Portfolio> getAllPortfolios() { return portfolios;}


    public String getId() {
        return userID;
    }

    public void replacePortfolio(Portfolio portfolio) throws ElementNotExists {
        for (Portfolio p: getAllPortfolios())
            if (p.getId().equals(portfolio.getId()))
            {
                getAllPortfolios().remove(p);
                getAllPortfolios().add(portfolio);
                return;
            }
        throw new ElementNotExists();
    }

    public void addPortfolio(Portfolio portfolio) throws ElementAlreadyExists {
        if (hasPortfolio(portfolio.getId()))
            throw new ElementAlreadyExists();
        portfolios.add(portfolio);
    }

    public boolean hasStock(String portfolioName) {
        for (Portfolio p: getAllPortfolios())
            if (p.hasStock(portfolioName))
                return true;
        return false;
    }

}