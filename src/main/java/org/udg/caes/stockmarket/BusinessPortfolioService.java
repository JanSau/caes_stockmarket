package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.exceptions.*;

/**
 * This class contains the business logic of the app.
 * It resembles a EJB from Java EE 7
 */



public class BusinessPortfolioService {
    PersistenceService mPS;
    StockService mSS;

    @Inject
    public BusinessPortfolioService(PersistenceService ps, StockService ss) {
        mPS = ps;
        mSS = ss;
    }

    Portfolio getPortfolio(String userId, String name) throws EntityNotFound, ElementNotExists {
        User u = mPS.getUser(userId);
        return u.getPortfolio(name);
    }


    void savePortfolio(Portfolio p) throws ElementNotExists, EntityNotFound, ElementAlreadyExists, InvalidOperation {
        mPS.savePortfolio(p);
    }


    Double getPortfolioValuation(String userId, String pname) throws Exception {
        User u = mPS.getUser(userId);
        Portfolio p = u.getPortfolio(pname);
        double v = 0.0;
        for (Stock s : p.getAllStocks()) {
            v += s.getQuantity() * mSS.getPrice(s.getName());
        }
        return v;
    }

    Portfolio getBestPortfolio(String userId) throws EntityNotFound, StockNotFound {
        User u = mPS.getUser(userId);
        double max = -Double.MAX_VALUE;
        Portfolio result = null;
        for (Portfolio p: u.getAllPortfolios()) {
            double profit = 0.0;
            for (Stock s : p.getAllStocks())
                profit += s.getQuantity() * (mSS.getPrice(s.getName()) - s.getBuyPrice());
            if (profit > max) {
                max = profit;
                result = p;
            }
        }
        return result;
    }
}
