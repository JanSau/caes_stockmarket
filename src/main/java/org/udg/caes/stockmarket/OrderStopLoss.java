package org.udg.caes.stockmarket;

import java.util.Date;

public class OrderStopLoss extends Order {
    private double lossPrice;

    public OrderStopLoss(Portfolio pf, int q, Date li, String t, double lp) {
        super(pf,q,li,t);
        lossPrice = lp;
    }

    public void setEfPrice(double ep) {
        super.setEfPrice(this.getLossPrice());
    }

    public double getLossPrice() {
        return lossPrice;
    }



}
