package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;

import java.util.ArrayList;
import java.util.List;

public class BusinessUserService {
    PersistenceService mPS;
    StockService mSS;

    @Inject
    public BusinessUserService(PersistenceService ps, StockService ss) {
        mPS = ps;
        mSS = ss;
    }

    public User getUser(String id) throws EntityNotFound {
        return mPS.getUser(id);
    }

    public void saveUser(User u) {
        mPS.saveUser(u);
    }

    public Double getUserValuation(String userId) throws Exception {
        User u = mPS.getUser(userId);
        double v = 0.0;
        for (Portfolio p: u.getAllPortfolios()) {
            for (Stock s : p.getAllStocks())
                v += s.getQuantity() * mSS.getPrice(s.getName());
        }
        return v;
    }

    public List<String> getCommonStocks(String userId1, String userId2) throws EntityNotFound {
        User u1 = mPS.getUser(userId1);
        User u2 = mPS.getUser(userId2);
        ArrayList<String> stocks = new ArrayList<String>();
        for (Portfolio p: u1.getAllPortfolios()) {
            for (Stock s : p.getAllStocks())
                if (!stocks.contains(s.getName()) && u2.hasStock(s.getName()))
                    stocks.add(s.getName());
        }
        return stocks;
    }


    public Boolean hasStock(String userId, String ticker) throws EntityNotFound {
        User u = mPS.getUser(userId);
        for (Portfolio p: u.getAllPortfolios()) {
            if (p.hasStock(ticker))
                return true;
        }
        return false;
    }

}
