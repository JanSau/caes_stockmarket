package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jasab_000 on 17/01/2015.
 */
public class StockManagerService {

    //Cal dividir en dos aquesta funció fent un refactor

    PersistenceService mPS;
    StockService mSS;
    private Integer orderId = 0;

    @Inject
    public StockManagerService(PersistenceService ps, StockService ss) {
        mPS = ps;
        mSS = ss;
    }

    public StockService getStockService() {
        return mSS;
    }


    public ArrayList<String> searchStocks(String substring) {
        ArrayList<String> r = new ArrayList<String>();

        for (StockMarket sm: mSS.getMarkets())
            for (String s : sm.getAllStocks())
                if (s.contains(substring))
                    r.add(s);

        return r;
    }

    public List<String> searchStocksWithValue(double price) {
        ArrayList<String> r = new ArrayList<String>();

        for (StockMarket sm: mSS.getMarkets())
            for (String s : sm.getAllStocks())
                try {
                    if (mSS.getPrice(s) > price)
                        r.add(s);
                } catch (StockNotFound stockNotFound) {
                }

        return r;
    }

}
