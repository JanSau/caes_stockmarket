package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.exceptions.*;

import java.util.List;

/**
 * This class contains the business logic of the app.
 * It resembles a EJB from Java EE 7
 */

public class BusinessService {
  PersistenceService mPS;
  StockService mSS;
  private Integer orderId = 0;

  BusinessPortfolioService portfolioService;
  BusinessUserService userService;

  @Inject
  public BusinessService(PersistenceService ps, StockService ss) {
    mPS = ps;
    mSS = ss;
    init();
  }

  void init(){
      userService = new BusinessUserService(mPS,mSS);
      portfolioService = new BusinessPortfolioService(mPS,mSS);
  }

  public User getUser(String id) throws EntityNotFound {
    return userService.getUser(id);
  }

  public Portfolio getPortfolio(String userId, String name) throws EntityNotFound, ElementNotExists {
    return portfolioService.getPortfolio(userId,name);
  }

  public void saveUser(User u) {
    userService.saveUser(u);
  }

  public void savePortfolio(Portfolio p) throws ElementNotExists, EntityNotFound, ElementAlreadyExists, InvalidOperation {
    portfolioService.savePortfolio(p);
  }

  public Boolean hasStock(String userId, String ticker) throws EntityNotFound {
    return userService.hasStock(userId,ticker);
  }

  public Double getUserValuation(String userId) throws Exception {
    return userService.getUserValuation(userId);
  }

  public Double getPortfolioValuation(String userId, String pname) throws Exception {
    return portfolioService.getPortfolioValuation(userId,pname);
  }

  public Portfolio getBestPortfolio(String userId) throws EntityNotFound, StockNotFound {
    return portfolioService.getBestPortfolio(userId);
  }

  public List<String> getCommonStocks(String userId1, String userId2) throws EntityNotFound {
    return userService.getCommonStocks(userId1,userId2);
  }

  /**
   * This method sends an Order for a give User
   *
   * @param o Order
   * @throws OrderNotSent
   */
  public void placeOrder(Order o) throws NoSuchMethodException {
    orderId++;
    o.setId(orderId.toString());
    o.setStatus(Order.PROCESSING);
    mPS.saveOrder(o);
    o.send(mSS);
  }


    public StockService getStockService() {
        return mSS;
  }

    public PersistenceService getPersistenceService() {
        return mPS;
    }

}
