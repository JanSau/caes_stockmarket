package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.persistence.Fake_PS_MySQL;

/**
 * Created by jasab_000 on 14/01/2015.
 */
public class OrderManagerService {


    public static PersistenceService getPersistenceService() {
        return mPS;
    }

    static PersistenceService mPS;

    static StockService mSS;
    private Integer orderId = 0;

    @Inject
    public OrderManagerService(PersistenceService ps, StockService ss) {
        mPS = ps;
        mSS = ss;
    }

    /**
     * This method sends an Order for a give User
     *
     * @param o Order
     * @throws org.udg.caes.stockmarket.exceptions.OrderNotSent
     */
    public void placeOrder(Order o) throws NoSuchMethodException {
        orderId++;
        o.setId(orderId.toString());
        o.setStatus(Order.PROCESSING);
        mPS.saveOrder(o);
        o.send(mSS);
    }


    public StockService getStockService() {
        return mSS;
    }
    /**
     * This method is called every time an Order has been processed. The order comes with the field status modified appropriately.
     * @param order Order processed
     */
    public static void processingCallback(Order order) {
        getPersistenceService().saveOrder(order);
        //.saveOrder(order);
    }



    /**
     * This method is called every time an Order has been executed. The order comes with the field status modified appropriately.
     * @param order Order processed
     */
    public static void executionCallback(Order order) {
        if (order.getStatus() == Order.COMPLETED || order.getStatus() == Order.PARTIALLY_COMPLETED) {
            int efq = order.getEfQuant();
            double efp = order.getEfPrice();
            String ticker = order.getTicker();
            Stock s = new Stock(ticker, efq, efp);
            order.getPortfolio().addStock(s);
        }
        getPersistenceService().saveOrder(order);
    }
}
